#Panda Analysis

##功能介绍

panda的主要的两个功能是对目标程序或系统进行记录和重放，然后再在重放过程中添加各种分析插件进行分析。

##记录和重放

panda的记录和重放功能是基于qemu实现的，在qemu的monitor中对应如下指令：begin_record，end_record，begin_replay，end_replay。其中重放功能可以在启动qemu时的参数中使用。

panda的重放与qemu本身的快照（snapshot）功能有所不同，在保存了.snp文件的基础上还保存了.log文件，该文件是panda所记录的执行信息，使用google的protocol buffer格式，基于该文件就可以从指令层次上重放记录，而不需要再重新启动镜像和图形界面，重放内容如下所示：

    opening nondet log for read :   ./cve-2011-1255-crash-rr-nondet.log
    ./cve-2011-1255-crash-rr-nondet.log:  324672 of 13204683 (2.46%) bytes, 14286548 of 1425929663 (1.00%) instructions processed.
    [...]
    ./cve-2011-1255-crash-rr-nondet.log:  3224618 of 13204683 (24.42%) bytes, 370747250 of 1425929663 (26.00%) instructions processed.
    ./cve-2011-1255-crash-rr-nondet.log:  3314428 of 13204683 (25.10%) bytes, 385204921 of 1425929663 (27.01%) instructions processed.

下面首先分析panda的记录功能的实现和相关函数

###记录 record

首先根据panda在qemu中添加的begin_record命令入手，在qemu中寻找begin_record命令的入口，在文件hmp-commands.h中找到如下代码：

		750 {
		751 .name       = "begin_record",
		752 .args_type  = "file_name:s",
		753 .params     = "[file_name]",
		754 .help       = "begin recording for later replay",
		755 .mhandler.cmd = hmp_begin_record,
		756 },

文件hmp-commands.h的作用是qemu所有命令的声明与关联到目标函数，上面的代码表示命令begin_record需要一个字符串参数file_name，并且关联函数为hmp_begin_record，该函数于qemu/hmp.h中声明，在qemu/rr_log.c中定义：

		1417 // HMP commands (the "monitor")
		1418 void hmp_begin_record(Monitor *mon, const QDict *qdict)
		1419 {
		1420   Error *err;
		1421   const char *file_name = qdict_get_try_str(qdict, "file_name");
		1422   qmp_begin_record(file_name, &err);
		1423 }

该函数首先获取参数file_name，然后调用函数qmp_begin_record，函数qmp_begin_record于qemu/qmp-commands.h中声明，在文件qemu/rr_log.c中定义：

		1382 void qmp_begin_record(const char *file_name, Error **errp) {
		1383   rr_record_requested = 1;
		1384   rr_requested_name = g_strdup(file_name);
		1385 }

该函数比较简单，仅仅将变量rr_record_requested赋值为1，将rr_requested_name赋值为传入的参数file_name，并没有后续调用其它函数，所以这里猜想变量rr_record_requested的值可能是作为一个标志，其为1时会使得某些函数或代码运行，变量rr_record_requested的定义如下：

		 105 volatile sig_atomic_t rr_record_requested = 0;

该变量在qemu/rr_log.c中定义，是一个全局变量。继续寻找使用该变量的代码，因为函数qmp_begin_record中将该变量赋值为1，所以我们要寻找的代码应该是一个条件判断，即判断rr_record_requested是否为1，结果如下：

		1457 //mz file_name_full should be full path to desired record/replay log file
		1458 int rr_do_begin_record(const char *file_name_full, void *cpu_state) {
		[......]
		1478   if (rr_record_requested  == 1 || rr_record_requested == 2) {
		1479     rr_get_snapshot_file_name(rr_name, rr_path, name_buf, sizeof(name_buf));
		1480     printf ("writing snapshot:\t%s\n", name_buf);
		1481     snapshot_ret = do_savevm_rr(get_monitor(), name_buf);
		1482     log_all_cpu_states();
		1483   }

对rr_record_requested进行判断的代码位于函数rr_do_begin_record中，该函数在rr_log.c中定义。可以看出，当rr_record_requested为1或2时，该函数会执行相应的代码进行记录。函数rr_do_begin_record被函数main_loop调用，函数main_loop被函数main调用，main函数就是qemu的主体入口函数。

函数rr_do_begin_record和do_savevm_rr均是panda自己添加的，原版的qemu中并没有这些函数，可能名字中函数rr的函数和变量都是panda新添加的。

函数do_savevm_rr的作用是调用qemu_savevm_state记录当前的vm状态。因此panda的记录功能主要是在函数log_all_cpu_states中。

	 	  362 //mz 05.2012 adding this for RR logging
		  363 void log_all_cpu_states(void) {
		  364     CPUState *env;
		  365     int flags;
		  366
		  367 #ifdef TARGET_I386
		  368     flags = X86_DUMP_FPU;
		  369 #else
		  370     flags = 0;
		  371 #endif
		  372
		  373     if (rr_debug_whisper()) {
		  374         fprintf(logfile, "\nLogging all cpu states\n");
		  375         for(env = first_cpu; env != NULL; env = env->next_cpu) {
		  376             fprintf(logfile, "CPU #%d:\n", env->cpu_index);
		  377             cpu_dump_state(env, logfile, fprintf, flags);
		  378         }
		  379     }
		  380     else {
		  381         printf ("\nLogging all cpu states\n");
		  382         for(env = first_cpu; env != NULL; env = env->next_cpu) {
		  383             printf("CPU #%d:\n", env->cpu_index);
		  384             cpu_dump_state(env, stderr, fprintf, flags);
		  385         }
		  386     }
		  387 }

该函数只是调用cpu_dump_state将当前的cpu状态输出到日志文件中，并且函数cpu_dump_state是qemu的自带函数，panda没有进行任何修改。

以上代码记录的是.snp文件中的内容，而另一个.log文件中的内容在函数rr_do_begin_record的下半部分执行：

		1485   // save the time so we can report how long record takes
		1486   time(&rr_start_time);
		1487
		1488   // second, open non-deterministic input log for write.
		1489   rr_get_nondet_log_file_name(rr_name, rr_path, name_buf, sizeof(name_buf));
		1490   printf ("opening nondet log for write :\t%s\n", name_buf);
		1491   rr_create_record_log(name_buf);
		1492   // reset record/replay counters and flags
		1493   rr_reset_state(cpu_state);
		1494   g_free(rr_path_base);
		1495   g_free(rr_name_base);
		1496   // set global to turn on recording
		1497   rr_mode = RR_RECORD;
		1498   //cpu_set_log(CPU_LOG_TB_IN_ASM|CPU_LOG_RR);
		1499   return snapshot_ret;
		1500 #endif
		1501 }

后面的代码首先调用rr_get_nondet_log_file_name创建.log文件，然后调用函数rr_create_record_log对.log文件进行写入操作

		1235 // create record log
		1236 void rr_create_record_log (const char *filename) {
		1237   // create log
		1238   rr_nondet_log = g_new0(RR_log, 1);
		1239   rr_assert (rr_nondet_log != NULL);
		1240
		1241   rr_nondet_log->type = RECORD;
		1242   rr_nondet_log->name = g_strdup(filename);
		1243   rr_nondet_log->fp = fopen(rr_nondet_log->name, "w");
		1244   rr_assert(rr_nondet_log->fp != NULL);
		1245
		1246   if (rr_debug_whisper()) {
		1247     fprintf (logfile, "opened %s for write.\n", rr_nondet_log->name);
		1248   }
		1249   //mz It would be very handy to know how "far" we are in a particular replay
		1250   //execution.  To do this, let's store a header in the log (we'll fill it in
		1251   //again when we close the log) that includes the maximum instruction
		1252   //count as a monotonicly increasing measure of progress.
		1253   //This way, when we print progress, we can use something better than size of log consum     ed
		1254   //(as that can jump //sporadically).
		1255   fwrite(&(rr_nondet_log->last_prog_point), sizeof(RR_prog_point), 1, rr_nondet_log->fp);
		1256 }

该函数首先对全局变量rr_nodet_log进行赋值，然后将其中的last_prog_point成员变量写入.log文件，该变量是RR_prog_point类型的，包含的内容有：

		 72 //mz 10.20.2009
		 73 //mz A record of a point in the program.  This is a subset of guest CPU state
		 74 //mz and the number of guest instructions executed so far. 75 typedef struct RR_prog_point_t {
		 76   uint64_t pc;
		 77   uint64_t secondary;
		 78   uint64_t guest_instr_count;
		 79 } RR_prog_point;

其中guest_instr_count代表到目前为止所执行的指令的数目，这样就可以知道执行到程序的什么位置。

接下来分析end_record功能，和begin_record一样，end_record也是通过命令调用，然后跳转到函数hmp_end_record，然后是qmp_end_record，并将变量rr_end_record_requested赋值为1。最终使用到该变量的函数为do_end_record:

		1504 void rr_do_end_record(void) {
		1505 #ifdef CONFIG_SOFTMMU
		1506   //mz put in end-of-log marker
		1507   rr_record_end_of_log();
		1508
		1509   char *rr_path_base = g_strdup(rr_nondet_log->name);
		1510   char *rr_name_base = g_strdup(rr_nondet_log->name);
		1511   //char *rr_path = dirname(rr_path_base);
		1512   char *rr_name = basename(rr_name_base);
		1513
		1514   if (rr_debug_whisper()) {
		1515     fprintf (logfile,"End vm record for name = %s\n", rr_name);
		1516     printf ("End vm record for name = %s\n", rr_name);
		1517   }
		1518
		1519   time_t rr_end_time;
		1520   time(&rr_end_time);
		1521   printf("Time taken was: %ld seconds.\n", rr_end_time - rr_start_time);
		1522
		1523   log_all_cpu_states();
		1524
		1525   rr_destroy_log();
		1526
		1527   g_free(rr_path_base);
		1528   g_free(rr_name_base);
		1529
		1530   // turn off logging
		1531   rr_mode = RR_OFF;
		1532 #endif
		1533 }

该函数在确认结束记录后，调用函数log_all_cpu_states输出当前的cpu状态，然后调用rr_destroy_log关闭记录文件并释放相关内存，最后将全局标志rr_mode设置为RR_OFF表示记录功能关闭。

		1283 // close file and free associated memory
		1284 void rr_destroy_log(void) {
		1285   if (rr_nondet_log->fp) {
		1286     //mz if in record, update the header with the last written prog point.
		1287     if (rr_nondet_log->type == RECORD) {
		1288         rewind(rr_nondet_log->fp);
		1289         fwrite(&(rr_nondet_log->last_prog_point), sizeof(RR_prog_point), 1, rr_nondet_log     ->fp);
		1290     }
		1291     fclose(rr_nondet_log->fp);
		1292     rr_nondet_log->fp = NULL;
		1293   }
		1294   g_free(rr_nondet_log->name);
		1295   g_free(rr_nondet_log);
		1296   rr_nondet_log = NULL;
		1297 }

函数rr_destroy_log中，首先根据全局变量rr_nondet_log获得记录文件的指针，然后将当前的最后一条记录写入文件中并关闭文件，释放相关变量的内存。

** panda record内容分析 **

上面介绍的是panda如何在qemu中开启record功能，真正实现panda record内容的函数并不在上面介绍的函数中。根据之前的分析，panda在开启record功能时会将rr_mode赋值为RR_RECORD，而panda中存在一个函数rr_in_record，这个函数的作用是判断当前是否处于record模式。

		578 static inline uint8_t rr_in_record(void) {
		579   return (rr_mode == RR_RECORD);
		580 }

这个函数仅是通过rr_mode来判断当前属于哪种模式。因此，通过分析那些地方调用了这个函数，就可以找到实现record内容的操作。通过分析，有三处调用该函数的代码与record内容相关，这三处代码分别位于三个函数中：cpu_register_physical_memory_log，cpu_physical_memory_rw_ex和cpu_physical_memory_unmap。

cpu_register_physical_memory_log中的代码如下：

		2796 #ifdef CONFIG_SOFTMMU
		2797     if (rr_in_record() && rr_record_in_progress) {
		2798         rr_reg_mem_call_record(start_addr, size, phys_offset);
		2799     }
		2800 #endif

cpu_physical_memory_rw_ex中的代码如下：

		4201                 //mz record & replay
		4202                 //mz Need to record parameters of this call in the log (so that we can
		4203                 //replay it) if it occurs during main_loop_wait() call
		4204                 //mz Only care about writes to memory.
		4205                 if (rr_in_record() && rr_record_in_progress) {
		4206                    rr_device_mem_rw_call_record(addr, buf, len, is_write);
		4207                 }

cpu_physical_memory_unmap中的代码如下：

		4441             //bdg Save addr1,access_len,buffer contents
		4442             if (rr_in_record() && rr_record_in_progress) {
		4443                 rr_cpu_physical_memory_unmap_record(addr1, buffer, access_len, is_write);
		4444             }

这三处代码分别调用了不同的函数来记录各自的内容。上面代码所调用的三个函数定义如下：

		 48 static inline void rr_cpu_physical_memory_unmap_record(target_phys_addr_t addr, uint8_t *buf, target_phys_addr_t len, int is_write) {
   	     49   rr_record_cpu_mem_unmap((RR_callsite_id) rr_skipped_callsite_location, addr, buf, len, is_write);
	     50 }
	     51
		 52 //mz XXX addr should be target_phys_addr_t
		 53 static inline void rr_device_mem_rw_call_record(target_phys_addr_t addr, uint8_t *buf, int len, int is_write) {
		 54     rr_record_cpu_mem_rw_call((RR_callsite_id) rr_skipped_callsite_location, addr, buf, len, is_write);
		 55 }
		 56
		 57 //mz XXX addr should be target_phys_addr_t
		 58 static inline void rr_reg_mem_call_record(target_phys_addr_t start_addr, ram_addr_t size, ram_addr_t phys_offset) {
         59     rr_record_cpu_reg_io_mem_region((RR_callsite_id) rr_skipped_callsite_location, start_a ddr, size, phys_offset);
		 60 }

最终调用的函数是rr_record_cpu_mem_unmap，rr_record_cpu_mem_rw_call和rr_record_cpu_reg_io_mem_region，这三个函数是构造并输出record内容的函数，他们在rr_log.c中定义，除了这三个函数之外，rr_log.c中还定义了若干个record函数，分别用来记录不同的内容，每个函数的实现过程均类似，使用类型RR_log_entry作为记录内容的类型，根据不同的内容在variant域进行赋值，然后写入到文件中。

variant域是一个联合体：

		 86 // an item in a program-point indexed record/replay log
		 87 typedef struct rr_log_entry_t {
	     88     RR_header header;
	     89     //mz all possible options, depending on log_entry.kind
		 90     union {
		 91         // if log_entry.kind == RR_INPUT_1
		 92         uint8_t input_1;
		 93         // if log_entry.kind == RR_INPUT_2
		 94         uint16_t input_2;
		 95         // if log_entry.kind == RR_INPUT_4
		 96         uint32_t input_4;
		 97         // if log_entry.kind == RR_INPUT_8
		 98         uint64_t input_8;
	     99         // if log_entry.kind == RR_INTERRUPT_REQUEST
		100         uint16_t interrupt_request;         //mz 2-bytes is enough for the interrupt request value!
		101         // if log_entry.kind == RR_EXIT_REQUEST
		102         uint16_t exit_request;
		103         // if log_entry.kind == RR_SKIPPED_CALL
		104         RR_skipped_call_args call_args;
		105         // if log_entry.kind == RR_LAST
		106         // no variant fields
		107     } variant;
		108     struct rr_log_entry_t *next;109 } RR_log_entry;

因此不同的记录内容可以根据其中不同的成员来选择存放格式。

###重放 replay

panda中和重放相关的指令为begin_replay和end_replay。

begin_replay对应的处理函数为hmp_begin_replay --> qmp_begin_replay，在qmp_begin_replay中，将全局变量rr_replay_requested赋值为1，这点和begin_record一致。

		1393 void qmp_begin_replay(const char *file_name, Error **errp) {
		1394   rr_replay_requested = 1;
		1395   rr_requested_name = g_strdup(file_name);
		1396   gettimeofday(&replay_start_time, 0);
		1397 }

变量rr_replay_requested在很多地方被调用，其中比较重要的地方有：

		1023 static int tcg_cpu_exec(CPUState *env)
		1024 {
		1025     int ret;
		1026 #ifdef CONFIG_PROFILER
		1027     int64_t ti;
		1028 #endif
		1029     // Don't run code if we're about to record from an existing
		1030     // snapshot or replay a recording.
		1031     // Don't waste effort running guest code when we're about
		1032     // to load a VMstate anyway. Especially useful for recording
		1033     // or replaying from boot or the command line.
		1034     // It probably wouldn't hurt to also return for
		1035     // rr_record_requested == 1 (record from current state)
		1036     if(2 == rr_record_requested || 0 != rr_replay_requested){
		1037         return EXCP_HALTED;
		1038     }

这里表示如果当前在一个存在的快照中进行记录，或者重放一个记录时，不需要运行目标代码，直接跳过。

panda在重放模式中禁用掉了一些功能，为了保证重放模式不出现问题，这些代码比较散，一般是通过如下判断语句来识别是否处于重放模式：

		142     if (rr_in_replay() || rr_replay_requested) {
		143         return;
		144     }

最重要的代码在函数main_loop中：

		1570         if (__builtin_expect(rr_replay_requested, 0)) {
		1571             //block signals
		1572             sigprocmask(SIG_BLOCK, &blockset, &oldset);
		1573             if (0 != rr_do_begin_replay(rr_requested_name, first_cpu)){
		1574                 printf("Failed to start replay\n");
		1575                 fix_replay_stuff();
		1576             } else { // we have to unblock signals, so we can't just continue on failure
		1577                 quit_timers();
		1578                 rr_replay_requested = 0;
		1579             }
		1580             //unblock signals
		1581             sigprocmask(SIG_SETMASK, &oldset, NULL);
		1582         }

如果当前的rr_replay_requested不为0，则执行上述代码，宏__builtin_expect是针对编译器的分支预测，表示该变量很大概率等于后面的值。进入之后首先调用函数sigprocmask将信号屏蔽，然后运行函数rr_do_begin_replay，并判断其返回值是否为0，为0表示失败。无论成功还是失败最后都需要调用函数sigprocmask将之前屏蔽掉的信号打开。

这部分最重要的是函数rr_do_begin_replay的实现。

函数rr_do_begin_replay主要做了两件事情：（1）加载需要重放的快照，恢复快照保存的cpu状态；（2）加载log文件并设置当前模式为重放模式。

		1552   // first retrieve snapshot
		1553   rr_get_snapshot_file_name(rr_name, rr_path, name_buf, sizeof(name_buf));
		1554   if (rr_debug_whisper()) {
		1555     fprintf (logfile,"reading snapshot:\t%s\n", name_buf);
		1556   }
		1557   printf ("loading snapshot\n");
		1558   //  vm_stop(0) RUN_STATE_RESTORE_VM);
		1559     panda_cb_list *plist;
		1560     for(plist = panda_cbs[PANDA_CB_BEFORE_REPLAY_LOADVM]; plist != NULL;
		1561             plist = panda_cb_list_next(plist)) {
		1562         plist->entry.before_loadvm();
		1563     }
		1564   snapshot_ret = load_vmstate_rr(name_buf);

上面的代码就是用来加载快照并恢复cpu状态的。其中的for循环，目前具体的用途还不是很明白，但应该是用来设置回调函数的，before_loadvm是一个函数指针。

		1577   // second, open non-deterministic input log for read.
		1578   rr_get_nondet_log_file_name(rr_name, rr_path, name_buf, sizeof(name_buf));
		1579   printf ("opening nondet log for read :\t%s\n", name_buf);
		1580   rr_create_replay_log(name_buf);
		1581   // reset record/replay counters and flags
		1582   rr_reset_state(cpu_state);
		1583   // set global to turn on replay
		1584   rr_mode = RR_REPLAY;
		1585
		1586   //cpu_set_log(CPU_LOG_TB_IN_ASM|CPU_LOG_RR);
		1587
		1588   //mz fill the queue!
		1589   rr_fill_queue();

这部分代码就是加载log文件和设置当前模式为重放模式的代码。标记当前模式的是全局变量rr_mode。最后有一个函数调用rr_fill_queue，该函数的作用是根据log文件的内容填充log entry的队列。log entry是panda自己定义的类型，目前推测和其记录的储存方式有关系，但具体关系还不太明白。

根据当前的变量rr_mode已经被赋值为重放模式，因此需要了解这个赋值会影响到哪些代码，根据搜索，panda中共有9个地方使用到了判定表达式rr_mode == RR_REPLAY，其中比较重要的地方有：

文件cpu-exec.c中的函数cpu_exec：

		829                 // Check for termination in replay
		830                 if (rr_mode == RR_REPLAY && rr_replay_finished()) {
		831                     rr_end_replay_requested = 1;
		832                     break;
		833                 }

这里是判断重放是否结束，如果结束，那么将rr_end_replay_requested赋值为1，表示关闭重放模式。

文件exec.c中的函数cpu_physical_memory_rw_ex：

		4185                 if (rr_mode == RR_REPLAY) {
		4186                     // run all callbacks registered for cpu_physical_memory_rw ram case
		4187                     panda_cb_list *plist;
		4188                     for (plist = panda_cbs[PANDA_CB_REPLAY_BEFORE_CPU_PHYSICAL_MEM_RW_RAM     ]; plist != NULL; plist = panda_cb_list_next(plist)) {
		4189                         plist->entry.replay_before_cpu_physical_mem_rw_ram(cpu_single_env     , is_write, buf, addr1, l);
		4190                     }
		4191                 }

根据该函数的名称可以猜到，该函数是在系统进行物理内存访问时才会执行的，这里对重放模式进行判断，如果是重放模式，那么就运行所有的关于物理内存访问的回调函数，也就是相关的分析函数。


end_replay的调用过程和begin_replay基本一致，最终是将变量rr_end_replay_requested赋值为1，但由于重放模式一般都是自动结束的，之前的分析中也提到过，所以这里只分析一下函数rr_do_end_replay。

函数rr_do_end_replay所做的事情基本上和函数rr_do_begin_replay相反，例如：清空log entry队列，关闭log文件，将模式修改为关闭模式。除此之外，该函数最后还会调用函数qemu_system_shutdown_request关闭qemu。

## Pandalog

###结构分析

Pandalog是panda使用google的protocol buffer创建的记录文件规格。

protocol buffer在使用时需要创建一个格式文件，根据这个文件，protocol buffer可以生成对应的C++或java的对应实现文件。panda中创建的格式文件是pandalog.proto，生成的C++实现文件是pandalog.pb-c.h和pandalog.pb-c.c。

pandalog.proto中的部分内容如下：

		  9 message SrcInfo {
		 10     required string filename = 1;
		 11     required uint32 linenum = 2;
		 12     required string astnodename = 3;
		 13 }

其中message是protocol buffer所定义的类型，这是最基本的类型，有点像struct。然后是里面定义的内容，required属性表示该成员是必须的，除此之外还有repeated和optional属性。repeated属性表示该成员是可以重复的，也可以为0，一般用来表示数组；optional表示该属性是可选的。等号后面的数字表示该成员的顺序，也就是布局位置，位置在1-15之内的成员可以得到优化，因此尽量让repeated属性的成员的顺序位于1-15的范围。

protocol buffer还支持枚举类型，并且可以作为message的成员类型：

		enum UserStatus {
			OFFLINE = 0;  //表示处于离线状态的用户
			ONLINE = 1;   //表示处于在线状态的用户
		}

用法和C语言中的枚举类型一致，区别在于使用分号来分隔每个成员。

根据pandalog.proto生成的pandalog.pb-c.c和pandalog.pb-c.h是将之前定义的message转换成C++中的类型，并提供相应的处理函数，如上面举例的SrcInfo：

		 43 struct  _Panda__SrcInfo
		 44 {
		 45   ProtobufCMessage base;
		 46   char *filename;
		 47   uint32_t linenum;
		 48   char *astnodename;
		 49 };
		 50 #define PANDA__SRC_INFO__INIT \
		 51  { PROTOBUF_C_MESSAGE_INIT (&panda__src_info__descriptor) \
		 52     , NULL, 0, NULL }

这是SrcInfo在pandalog.pb-c.h中的定义，可以看到filename, linenum, astnodename都转换成了C++中的类型，而base是每个类型都会存在的成员，并且还在下面提供了用来初始化的宏。

		243 /* Panda__SrcInfo methods */
		244 void   panda__src_info__init
		245                      (Panda__SrcInfo         *message);
		246 size_t panda__src_info__get_packed_size
		247                      (const Panda__SrcInfo   *message);
		248 size_t panda__src_info__pack
		249                      (const Panda__SrcInfo   *message,
		250                       uint8_t             *out);
		251 size_t panda__src_info__pack_to_buffer
		252                      (const Panda__SrcInfo   *message,
		253                       ProtobufCBuffer     *buffer);
		254 Panda__SrcInfo *
		255        panda__src_info__unpack
		256                      (ProtobufCAllocator  *allocator,
		257                       size_t               len,
		258                       const uint8_t       *data);
		259 void   panda__src_info__free_unpacked
		260                      (Panda__SrcInfo *message,
		261                       ProtobufCAllocator *allocator);

上面的函数是protocol buffer为SrcInfo提供的处理函数，包含初始化、获取打包后的大小、pack、将pack转换到buffer、unpack、释放unpack后所占用的内存这6种处理函数，每个类型均存在这6中处理函数。

除此之外，protocol buffer还定义了与每个类型相关的描述符结构：

		 619 static const ProtobufCFieldDescriptor panda__src_info__field_descriptors[3] =
		 620 {
		 621   {
		 622     "filename",
		 623     1,
		 624     PROTOBUF_C_LABEL_REQUIRED,
		 625     PROTOBUF_C_TYPE_STRING,
		 626     0,   /* quantifier_offset */
		 627     PROTOBUF_C_OFFSETOF(Panda__SrcInfo, filename),
		 628     NULL,
		 629     NULL,
		 630     0,            /* packed */
		 631     0,NULL,NULL    /* reserved1,reserved2, etc */
		 632   },
		 633   {
		 634     "linenum",
		 635     2,
		 636     PROTOBUF_C_LABEL_REQUIRED,
		 637     PROTOBUF_C_TYPE_UINT32,
		 638     0,   /* quantifier_offset */
		 639     PROTOBUF_C_OFFSETOF(Panda__SrcInfo, linenum),
		 640     NULL,
		 641     NULL,
		 642     0,            /* packed */
		 643     0,NULL,NULL    /* reserved1,reserved2, etc */
		 644   },
		 645   {
		 646     "astnodename",
		 647     3,
		 648     PROTOBUF_C_LABEL_REQUIRED,
		 649     PROTOBUF_C_TYPE_STRING,
		 650     0,   /* quantifier_offset */
		 651     PROTOBUF_C_OFFSETOF(Panda__SrcInfo, astnodename),
		 652     NULL,
		 653     NULL,
		 654     0,            /* packed */
		 655     0,NULL,NULL    /* reserved1,reserved2, etc */
		 656   },
		 657 };

panda__src_info__field_descriptors是用来描述类型中的成员的，它将每个结构成员按照类型ProtobufCFieldDescriptor描述了出来，src_info中共有三个成员，所以这里也定义了大小为3的数组。

		 658 static const unsigned panda__src_info__field_indices_by_name[] = {
		 659   2,   /* field[2] = astnodename */
		 660   0,   /* field[0] = filename */
		 661   1,   /* field[1] = linenum */
		 662 };

这个数组是将结构成员按照名称进行排序，并存放其在结构体中的索引位置。

		 663 static const ProtobufCIntRange panda__src_info__number_ranges[1 + 1] =
		 664 {
		 665   { 1, 0 },
		 666   { 0, 3 }
		 667 };

		 668 const ProtobufCMessageDescriptor panda__src_info__descriptor = 
		 669 {
		 670   PROTOBUF_C_MESSAGE_DESCRIPTOR_MAGIC,
		 671   "panda.SrcInfo",
		 672   "SrcInfo",
		 673   "Panda__SrcInfo",
		 674   "panda",
		 675   sizeof(Panda__SrcInfo),
		 676   3,
		 677   panda__src_info__field_descriptors,
		 678   panda__src_info__field_indices_by_name,
		 679   1,  panda__src_info__number_ranges,
		 680   (ProtobufCMessageInit) panda__src_info__init,
		 681   NULL,NULL,NULL    /* reserved[123] */
		 682 };

这个是用来描述整个结构类型的，包含了前面的三个类型和其他的一些信息。

为了方便在qemu中调用pandalog的功能，panda还在这些文件的外层又封装了一层代码，所在文件为pandalog.h和pandalog.c。pandalog.h中只声明了5个函数

		  8 // NB: there is only one panda json log
		  9 // so these fns dont return a Pandalog or pass one as a param
		 10 void pandalog_open(const char *path, const char *mode);
		 11 int  pandalog_close(void);
		 12
		 13 // write this element to pandpog.
		 14 // "asid", "pc", instruction count key/values
		 15 // b/c those will get added by this fn
		 16 void pandalog_write_entry(Panda__LogEntry *entry);
		 17
		 18 // read this element from pandalog.
		 19 // allocates memory, which caller will free
		 20 Panda__LogEntry *pandalog_read_entry(void);
		 21
		 22 // Must call this to free the entry returned by pandalog_read_entry
		 23 void pandalog_free_entry(Panda__LogEntry *entry);

其中，pandalog_open和pandalog_close分别用来打开或者关闭一个pandalog文件。pandalog_write_entry用来向文件中写入一个Panda__LogEntry类型的值，pandalog_read_entry用来读取Panda__LogEntry的值。pandalog_free_entry用来释放log文件所占用的内存。

值得单独提出的是，结构Panda__LogEntry是一个很大的结构，他包含了其他所有的结构，如Panda__SrcInfo等，并且拥有自己的属性。因此，panda在qemu中以Panda__LogEntry为基本类型对系统的执行信息进行记录。结合protocol buffer的高效利用空间，使得结果文件所占空间很小

###使用分析

##插件分析

panda拥有很多插件，位于panda/qemu/panda_plugins/目录下。插件之间可以存在依赖关系。

下面优先选择依赖关系比较简单或者没有依赖关系的插件进行分析。

###核心机制分析

panda的插件机制的核心代码主要位于panda_plugin.h和panda_plugin.c。

panda_plugin.h中定义了一个枚举类型panda_cb_type，也就是panda的回调类型

		 28 typedef enum panda_cb_type {
		 29     PANDA_CB_BEFORE_BLOCK_TRANSLATE,    // Before translating each basic block
		 30     PANDA_CB_AFTER_BLOCK_TRANSLATE,     // After translating each basic block
		 31     PANDA_CB_BEFORE_BLOCK_EXEC_INVALIDATE_OPT,    // Before executing each basic block (wi    th option to invalidate, may trigger retranslation)
		 32     PANDA_CB_BEFORE_BLOCK_EXEC,         // Before executing each basic block
		 33     PANDA_CB_AFTER_BLOCK_EXEC,          // After executing each basic block
		 34     PANDA_CB_INSN_TRANSLATE,    // Before an insn is translated
		 35     PANDA_CB_INSN_EXEC,         // Before an insn is executed
		 36     PANDA_CB_VIRT_MEM_READ,     // After each memory read (virtual addr.)
		 37     PANDA_CB_VIRT_MEM_WRITE,    // Before each memory write (virtual addr.)
		 38     PANDA_CB_PHYS_MEM_READ,     // After each memory read (physical addr.)
		 39     PANDA_CB_PHYS_MEM_WRITE,    // Before each memory write (physical addr.)
		 40     PANDA_CB_HD_READ,           // Each HDD read
		 41     PANDA_CB_HD_WRITE,          // Each HDD write
		 42     PANDA_CB_GUEST_HYPERCALL,   // Hypercall from the guest (e.g. CPUID)
		 43     PANDA_CB_MONITOR,           // Monitor callback
		 44     PANDA_CB_CPU_RESTORE_STATE,  // In cpu_restore_state() (fault/exception)
		 45     PANDA_CB_BEFORE_REPLAY_LOADVM,     // at start of replay, before loadvm
		 46 #ifndef CONFIG_SOFTMMU          // *** Only callbacks for QEMU user mode *** //
		 47     PANDA_CB_USER_BEFORE_SYSCALL, // before system call
		 48     PANDA_CB_USER_AFTER_SYSCALL,  // after system call (with return value)
		 49 #endif
		 50 #ifdef CONFIG_PANDA_VMI
		 51     PANDA_CB_VMI_AFTER_FORK,    // After returning from fork()
		 52     PANDA_CB_VMI_AFTER_EXEC,    // After returning from exec()
		 53     PANDA_CB_VMI_AFTER_CLONE,    // After returning from clone()
		 54 #endif
		 55     PANDA_CB_VMI_PGD_CHANGED,   // After CPU's PGD is written to
		 56     PANDA_CB_REPLAY_HD_TRANSFER,    // in replay, hd transfer
		 57     PANDA_CB_REPLAY_NET_TRANSFER,   // in replay, transfers within network card (currently     only E1000)
		 58     PANDA_CB_REPLAY_BEFORE_CPU_PHYSICAL_MEM_RW_RAM,  // in replay, just before RAM case of     cpu_physical_mem_rw
		 59     PANDA_CB_REPLAY_HANDLE_PACKET,    // in replay, packet in / out
		 60     PANDA_CB_LAST
		 61 } panda_cb_type;

这些回调类型是panda所支持的回调类型，panda的插件也是基于这些回调机制来实现的。除此之外还有一个联合体类型panda_cb，该类型是所有可能的回掉函数类型的联合，也就是在里面针对每种类型的回掉函数都有一个对应的函数指针：

		 63 // Union of all possible callback function types
		 64 typedef union panda_cb {
		 65     /* Callback ID: PANDA_CB_BEFORE_BLOCK_EXEC_INVALIDATE_OPT
		 66
		 67        before_block_exec_invalidate_opt: called before execution of every basic
		 68        block, with the option to invalidate the TB
		 69
		 70        Arguments:
		 71         CPUState *env: the current CPU state
		 72         TranslationBlock *tb: the TB we are about to execute
		 73
		 74        Return value:
		 75         true if we should invalidate the current translation block
		 76         and retranslate, false otherwise
		 77     */
		 78     bool (*before_block_exec_invalidate_opt)(CPUState *env, TranslationBlock *tb);

上面的代码就是针对PANDA_CB_BEFORE_BLOCK_EXEC_INVALIDATE_OPT类型的回调函数指针。


###callstack_instr

panda插件是默认使用c++编写的，所以在使用到panda中的头文件和为panda提供接口时需要兼容c语言

		 21 extern "C" {
		 22
		 23 #include "config.h"
		 24 #include "qemu-common.h"
		 25 #include "cpu.h"
		 26
		 27 #include "panda_plugin.h"
		 28 #include "panda_plugin_plugin.h"
		 29 #include "pandalog.h"
		 30
		 31 #include "callstack_instr.h"
		 32
		 33 bool translate_callback(CPUState *env, target_ulong pc);
		 34 int exec_callback(CPUState *env, target_ulong pc);
		 35 int before_block_exec(CPUState *env, TranslationBlock *tb);
		 36 int after_block_exec(CPUState *env, TranslationBlock *tb, TranslationBlock *next_tb);
		 37 int after_block_translate(CPUState *env, TranslationBlock *tb);
		 38
		 39 bool init_plugin(void *);
		 40 void uninit_plugin(void *);
		 41 }

插件的开始是从init_plugin开始，被卸载时调用uninit_plugin。

		418 bool init_plugin(void *self) {
		419     printf("Initializing plugin callstack_instr\n");
		420
		421     panda_cb pcb;
		422
		423     panda_enable_memcb();
		424     panda_enable_precise_pc();
		425
		426     pcb.after_block_translate = after_block_translate;
		427     panda_register_callback(self, PANDA_CB_AFTER_BLOCK_TRANSLATE, pcb);
		428     pcb.after_block_exec = after_block_exec;
		429     panda_register_callback(self, PANDA_CB_AFTER_BLOCK_EXEC, pcb);
		430     pcb.before_block_exec = before_block_exec;
		431     panda_register_callback(self, PANDA_CB_BEFORE_BLOCK_EXEC, pcb);
		432
		433     return true;
		434 }

在该函数中，首先创建panda_cb类型的变量pcb，然后调用函数panda_enable_memcb和panda_enable_precise_pc将相应的标志开启。接下来就是注册对应回调函数，这个插件共注册了三个回调函数，针对的回调类型分别是PANDA_CB_AFTER_BLOCK_TRANSLATE，PANDA_CB_AFTER_BLOCK_EXEC和PANDA_CB_BEFORE_BLOCK_EXEC；即块翻译之后，块执行之后和块执行之前。这样，在qemu执行到所注册的回调事件发生时就会调用该插件中的函数。

函数after_block_translate是在一个块被翻译完之后执行的回调函数：

		290 int after_block_translate(CPUState *env, TranslationBlock *tb) {
		291     call_cache[tb->pc] = disas_block(env, tb->pc, tb->size);
		292
		293     return 1;
		294 }

这个函数很简单，将每个翻译好的块的pc值和其所属的指令类型配对填到map类型变量call_cache中。函数disas_block的作用是返回当前pc指令的类型，指令类型是枚举类型instr_type，共有9种类型：

		65 enum instr_type {
		66   INSTR_UNKNOWN = 0,
		67   INSTR_CALL,
		68   INSTR_RET,
		69   INSTR_SYSCALL,
		70   INSTR_SYSRET,
		71   INSTR_SYSENTER,
		72   INSTR_SYSEXIT,
		73   INSTR_INT,
		74   INSTR_IRET,
		75 };

函数after_block_exec是在一个块被执行之后执行的回调函数：

		317 int after_block_exec(CPUState *env, TranslationBlock *tb, TranslationBlock *next) {
		318     instr_type tb_type = call_cache[tb->pc];
		319
		320     if (tb_type == INSTR_CALL) {
		321         stack_entry se = {tb->pc+tb->size,tb_type};
		322         callstacks[get_stackid(env,tb->pc)].push_back(se);
		323
		324         // Also track the function that gets called
		325         target_ulong pc, cs_base;
		326         int flags;
		327         // This retrieves the pc in an architecture-neutral way
		328         cpu_get_tb_cpu_state(env, &pc, &cs_base, &flags);
		329         function_stacks[get_stackid(env,tb->pc)].push_back(pc);
		330
		331         PPP_RUN_CB(on_call, env, pc);
		332     }
		333     else if (tb_type == INSTR_RET) {
		334         //printf("Just executed a RET in TB " TARGET_FMT_lx "\n", tb->pc);
		335         //if (next) printf("Next TB: " TARGET_FMT_lx "\n", next->pc);
		336     }
		337
		338     return 1;
		339 }

该函数根据当前块的pc值查找其指令类型（储存在call_cache中），如果类型是INSTR_CALL就构造stack_entry类型变量se，并将其压入callstacks中，之后获取cpu状态，并将pc值压入function_stacks中，最后调用函数PPP_RUN_CB。

函数before_block_exec是在一个块被执行之前执行的回调函数：

		296 int before_block_exec(CPUState *env, TranslationBlock *tb) {
		297     std::vector<stack_entry> &v = callstacks[get_stackid(env,tb->pc)];
		298     std::vector<target_ulong> &w = function_stacks[get_stackid(env,tb->pc)];
		299     if (v.empty()) return 1;
		300
		301     // Search up to 10 down
		302     for (int i = v.size()-1; i > ((int)(v.size()-10)) && i >= 0; i--) {
		303         if (tb->pc == v[i].pc) {
		304             //printf("Matched at depth %d\n", v.size()-i);
		305             v.erase(v.begin()+i, v.end());
		306
		307             PPP_RUN_CB(on_ret, env, w[i]);
		308             w.erase(w.begin()+i, w.end());
		309
		310             break;
		311         }
		312     }
		313
		314     return 0;
		315 }

该函数中，首先获取当前块的pc值所对应的callstacks和function_stacks的内容，分别复制给v和w，然后从顶部遍历v（最深10层），如果当前块的pc值等于v中遍历内容的pc值，就将v中从该内容起，到顶部的内容清空，调用PPP_RUN_CB之后将w中对应的内容也清空。这个函数的作用可能是为了避免递归调用导致记录的函数调用栈信息过于庞大。

###llvm_trace

根据代码注释中所介绍，llvm_trace插件的作用是捕获LLVM格式的trace信息，并存储到三个文件中：llvm-mod.bc, llvm-functions.log, llvm-memlog.log。

第一个文件llvm-mod.bc存放了每个guest翻译块的LLVM IR bitcode。

llvm-functions.log存放了LLVM函数的执行顺序和QEMU-user模式下的系统调用选择信息。

llvm-memlog.log记录了每次内存（CPU状态）访问，例如bitcode中的每个分支目标。

该插件假设获取的是一个完整的trace，所以LLVM会被禁用，bitcode模型会在执行结束后写入。也就是说llvm-mod.bc中的内容是在插件执行结束后写入的，也就是在函数uninit_plugin中。

####init_plugin

首先从初始化开始分析：

		285 bool init_plugin(void *self) {
		286     printf("Initializing plugin llvm_trace\n");
		287
		288     panda_arg_list *args = panda_get_args("llvm_trace");
		289     basedir = panda_parse_string(args, "base", "/tmp");
		290     tubtf_on = panda_parse_bool(args, "tubtf");
		291
		292     printf("llvm_trace using basedir=%s\n", basedir);

首先是对一些参数和输出文件路径进行初始化。

		314     panda_cb pcb;
		315     panda_enable_memcb();
		316     pcb.before_block_exec = before_block_exec;
		317     panda_register_callback(self, PANDA_CB_BEFORE_BLOCK_EXEC, pcb);
		318     pcb.after_block_exec = after_block_exec;
		319     panda_register_callback(self, PANDA_CB_AFTER_BLOCK_EXEC, pcb);
		320     pcb.phys_mem_read = phys_mem_read_callback;
		321     panda_register_callback(self, PANDA_CB_PHYS_MEM_READ, pcb);
		322     pcb.phys_mem_write = phys_mem_write_callback;
		323     panda_register_callback(self, PANDA_CB_PHYS_MEM_WRITE, pcb);
		324     pcb.cb_cpu_restore_state = cb_cpu_restore_state;
		325     panda_register_callback(self, PANDA_CB_CPU_RESTORE_STATE, pcb);

然后是注册回调函数，共有5种：before_block_exec，after_block_exec，phys_mem_read_callback，phys_mem_write_callback，cb_cpu_restore_state。

		342     llvm::Module *mod = tcg_llvm_ctx->getModule();
		343     for (llvm::Module::iterator i = mod->begin(); i != mod->end(); i++){
		344         if (i->isDeclaration()){
		345             continue;
		346         }
		347 #if defined(TARGET_ARM)
		[...]
		355 #endif
		356         PIFP->runOnFunction(*i);
		357     }
		358     std::string err;
		359     if(verifyModule(*mod, llvm::AbortProcessAction, &err)){
		360         printf("%s\n", err.c_str());
		361         exit(1);
		362     }

回调函数注册完毕后，对每个翻译好的llvm module运行PIFP pass，PIFP全称为PandaInstrFunctionPass，作用是在目标代码中插桩，用来记录动态值（dynamic value）。插桩完毕后调用函数verifyModule对修改过的module进行验证，防止插桩过程破坏module的结构并导致无法执行。

####before_block_exec

函数before_block_exec在每个基本块被执行前运行：

		144 int before_block_exec(CPUState *env, TranslationBlock *tb){
		145
		146   if (tubtf_on) {
		147     char *llvm_fn_name = (char *) tcg_llvm_get_func_name(tb);
		148     uint32_t pc, unk;
		149     sscanf(llvm_fn_name, "tcg-llvm-tb-%d-%x", &unk, &pc);
		150     env->panda_guest_pc = pc;
		151     tubtf_write_el_64(panda_current_asid(env), pc, TUBTFE_LLVM_FN, unk, panda_in_kernel(en    v), 0, 0);152   }
		153   else {
		154     fprintf(funclog, "%s\n", tcg_llvm_get_func_name(tb));
		155     DynValBuffer *dynval_buffer = PIFP->PIV->getDynvalBuffer();
		156     if (dynval_buffer->cur_size > 0){
		157         // Buffer wasn't flushed before, have to flush it now
		158       fwrite(dynval_buffer->start, dynval_buffer->cur_size, 1, memlog);
		159     }
		160     clear_dynval_buffer(dynval_buffer);
		161   }
		162     return 0;
		163 }

该函数根据tubtf_on的值会有不同的处理。tubtf_on表示使用TUBTF格式进行存储，TUBTF全称为Tim's Uncomplicated Binary Trace Format。是一种简单的固定宽度二进制执行trace文件格式。

这个函数主要记录的内容函数的名称，pc值等。在tubtf_on为0的情况下，记录写入llvm-functions.log，否则记录写入tubtf记录文件中。

####after_block_exec

该函数是在块执行结束后运行的

		165 int after_block_exec(CPUState *env, TranslationBlock *tb,
		166         TranslationBlock *next_tb){
		167   if (tubtf_on == 0) {
		168     // flush dynlog to file
		169     assert(memlog);
		170     DynValBuffer *dynval_buffer = PIFP->PIV->getDynvalBuffer();
		171     fwrite(dynval_buffer->start, dynval_buffer->cur_size, 1, memlog);
		172     clear_dynval_buffer(dynval_buffer);
		173   }
		174     return 0;
		175 }

该函数只有在tubtf_on为0的情况下才进行处理，内容是将dynval_buffer的内容写入llvm-memory.log中。

####phys_mem_read_callback

该函数在读取物理内存时调用

		102 int phys_mem_read_callback(CPUState *env, target_ulong pc, target_ulong addr,
		103         target_ulong size, void *buf){
		104     DynValBuffer *dynval_buffer = PIFP->PIV->getDynvalBuffer();
		105     log_dynval(dynval_buffer, ADDRENTRY, LOAD, addr);
		106     return 0;
		107 }

主要内容是调用函数log_dynval将dynval_buffer中的内容纪录起来，使用log_dynval时需要告诉其要记录的是什么类型的内容，例如本函数中记录的内容是ADDRENTRY和LOAD，表示记录的是一个地址，并且是读取时使用的地址。

####phys_mem_write_callback

		 95 int phys_mem_write_callback(CPUState *env, target_ulong pc, target_ulong addr,
		 96                        target_ulong size, void *buf) {
		 97     DynValBuffer *dynval_buffer = PIFP->PIV->getDynvalBuffer();
		 98     log_dynval(dynval_buffer, ADDRENTRY, STORE, addr);
	     99     return 0;
		100 }

本函数记录的是对物理内存进行写入操作的信息，同样也是调用log_dynval进行记录。

函数phys_mem_read_callback和phys_mem_write_callback是针对全系统模式（whole-system mode）的，用户模式（user-mode）下的内存访问由IR插桩（IR instrumentation）捕获。

####cb_cpu_restore_state

该函数在cpu重置时会被调用

		177 int cb_cpu_restore_state(CPUState *env, TranslationBlock *tb){
		178     printf("EXCEPTION - logging\n");
		179     DynValBuffer *dynval_buffer = PIFP->PIV->getDynvalBuffer();
		180     log_exception(dynval_buffer);
		181     return 0;
		182 }

该函数需要记录的信息也是在dynval_buffer中，调用函数log_exception来记录，通过函数名称可以看出，这个函数记录的内容是cpu异常时的状态，所以存储的信息应该是发生异常时cpu的各个寄存器的内容。

###taint2

污点分析插件

####init_plugin

注册了两种类型的回调：PANDA_CB_GUEST_HYPERCALL和PANDA_CB_BEFORE_BLOCK_EXEC_INVALIDATE_OPT。PANDA_CB_GUEST_HYPERCALL类型的回调在函数helper_cpuid中执行。

该插件依赖插件callstack_instr。

####guest_hypercall_callback

根据不同的架构该函数会调用对应的处理函数，i386架构下会调用函数i386_hypercall_callback。

		532 void i386_hypercall_callback(CPUState *env){
		583     if (pandalog && env->regs[R_EAX] == 0xabcd) {
		584         // LAVA Hypercall
		585         target_ulong addr = panda_virt_to_phys(env, ECX);
		586         if ((int)addr == -1) {
		587             printf ("panda hypercall with ptr to invalid PandaHypercallStruct: vaddr=0x%x     paddr=0x%x\n",
		588                     (uint32_t) ECX, (uint32_t) addr);
		589         }
		590         else {
		591             PandaHypercallStruct phs;
		592             panda_virtual_memory_rw(env, ECX, (uint8_t *) &phs, sizeof(phs), false);
		593             if  (phs.action == 11) {
		594                 // it's a lava query
		595                 lava_taint_query(phs);
		596             }
		597             if (phs.action == 12) {
		598                 // it's an attack point sighting
		599                 lava_attack_point(phs);
		600             }
		601         }
		602     }
		603 }

####before_block_exec_invalidate_opt

		868 bool before_block_exec_invalidate_opt(CPUState *env, TranslationBlock *tb) {
		877     if (taintEnabled) {
		878         if (!tb->llvm_tc_ptr) {
		879             return true;
		880         } else {
		881             //tb->llvm_function->dump();
		882             return false;
		883         }
		884     }
		885     return false;
		886 }

该函数本身没有做什么事情，但是其使用到的变量taintEnabled值得注意。该变量的默认值是false，在函数__taint2_enable_taint中被赋值为true。

####__taint2_enable_taint

该函数注册了许多回调函数：

		200     pcb.after_block_translate = after_block_translate;
		201     panda_register_callback(plugin_ptr, PANDA_CB_AFTER_BLOCK_TRANSLATE, pcb);
		202     pcb.before_block_exec_invalidate_opt = before_block_exec_invalidate_opt;
		203     panda_register_callback(plugin_ptr, PANDA_CB_BEFORE_BLOCK_EXEC_INVALIDATE_OPT, pcb);
		204     pcb.before_block_exec = before_block_exec;
		205     panda_register_callback(plugin_ptr, PANDA_CB_BEFORE_BLOCK_EXEC, pcb);
		206     pcb.after_block_exec = after_block_exec;
		207     panda_register_callback(plugin_ptr, PANDA_CB_AFTER_BLOCK_EXEC, pcb);
		208     pcb.phys_mem_read = phys_mem_read_callback;
		209     panda_register_callback(plugin_ptr, PANDA_CB_PHYS_MEM_READ, pcb);
		210     pcb.phys_mem_write = phys_mem_write_callback;
		211     panda_register_callback(plugin_ptr, PANDA_CB_PHYS_MEM_WRITE, pcb);

上面共有6中回调被注册，但经过分析，这六个函数并没有太大作用，甚至有一个函数是空函数，因此这几个函数的真是作用还有待研究。

		234     shadow = tp_init(TAINT_BYTE_LABEL, TAINT_GRANULARITY_BYTE);
		235     if (shadow == NULL){
		236         printf("Error initializing shadow memory...\n");
		237         exit(1);
		238     }

然后初始化污点处理器。其中shadow是在文件taint2.cpp中定义的全局变量

		135 Shad *shadow = NULL; // Global shadow memory

类型为Shad，作为影子内存记录污点信息。

		240     // Initialize memlog.
		241     memset(&taint_memlog, 0, sizeof(taint_memlog));

初始化memlog。

		243     llvm::Module *mod = tcg_llvm_ctx->getModule();
		244     FPM = tcg_llvm_ctx->getFunctionPassManager();
		245
		246     // Add the taint analysis pass to our taint pass manager
		247     PTFP = new llvm::PandaTaintFunctionPass(shadow, &taint_memlog);
		248     FPM->add(PTFP);
		249
		250     if (optimize_llvm) {
		251         printf("taint2: Adding default optimizations (-O1).\n");
		252         llvm::PassManagerBuilder Builder;
		253         Builder.OptLevel = 1;
		254         Builder.SizeLevel = 0;
		255         Builder.populateFunctionPassManager(*FPM);
		256     }
		257
		258     FPM->doInitialization();
		259
		260     // Populate module with helper function taint ops
		261     for (auto i = mod->begin(); i != mod->end(); i++){
		262         if (!i->isDeclaration()) PTFP->runOnFunction(*i);
		263     }
		264
		265     printf("taint2: Done processing helper functions for taint.\n");

获取当前的llvm module和FunctionPassManager，创建PandaTaintFunctionPass的对象PTFP。然后对module中的每个函数执行PandaTaintFunctionPass。


